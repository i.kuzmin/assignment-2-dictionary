ASM=nasm
ASMFLAGS=-f elf64 -g
LD=ld

program: main.o lib.o dict.o
	$(LD) -o program $^

main.asm: words.inc dict.asm
	touch $@

dict.asm: lib.asm
	touch $@

words.inc: colon.inc
	touch $@

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

.PHONY: clean

clean:
	rm *.o
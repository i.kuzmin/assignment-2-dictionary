%include "words.inc"
%include "lib.inc"
%include "dict.inc"

%define BUFFER_SIZE 256

global _start

section .rodata
size_error_string: db "String size is larger than buffer", 0
not_found_error_string: db "Key not found", 0

section .bss
buffer: resb BUFFER_SIZE

section .text
_start:
    mov rdi, buffer
    mov rsi, 256

    call read_word
    test rax, rax
    je .size_error

    mov rdi, rax
    mov rsi, first_word
    call find_word
    test rax, rax
    je .not_found_error

    add rax, 8
    mov rdi, rax
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    call exit

.size_error:
    mov rdi, size_error_string
    jmp .print_error

.not_found_error:
    mov rdi, not_found_error_string
    jmp .print_error

.print_error:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, 2
    syscall
    jmp exit

%include "words.inc"
global find_word
extern string_equals

section .text
find_word:
.loop:
    push rdi
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    pop rdi
    cmp rax, 1
    je .found

    mov rsi, [rsi]
    test rsi, rsi
    jne .loop

    ; Not found
    xor rax, rax
    ret

.found:
    mov rax, rsi
    ret


